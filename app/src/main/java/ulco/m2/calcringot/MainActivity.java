package ulco.m2.calcringot;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    // Liste d'attributs pour contrôler la calculatrice.
    private RadioGroup radioGroup;
    private EditText value1Field;
    private EditText value2Field;
    private Button resultButton;
    private TextView resultText;
    private Button razButton;
    private Button quitButton;
    boolean canCompute;
    double tmpValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Ajout d'un icone dans la barre d'application.
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.mipmap.icon_foreground);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Initialisation des attributs ici.
        findViews();
        // Gestion des événements
        createEventHandlers();
        canCompute = true;
    }

    private void findViews(){
        radioGroup = findViewById(R.id.operators);
        value1Field = findViewById(R.id.editTextValue1);
        value2Field = findViewById(R.id.editTextValue2);
        resultButton = findViewById(R.id.equal);
        razButton = findViewById(R.id.raz);
        quitButton = findViewById(R.id.quit);
        resultText = findViewById(R.id.result);
    }

    private void createEventHandlers(){
        // Gestion de la valeur 1.
        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                // Si l'utilisateur commence par un point, on ajoute un 0 juste devant.
                if(s.toString().startsWith("."))
                    s.insert(0, "0");
            }
        };
        // Gestion de la valeur 2.
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // On tente un parseDouble directement lors du changement.
                try {
                    tmpValue = Double.parseDouble(s.toString());
                    canCompute = tmpValue != 0.0 || radioGroup.getCheckedRadioButtonId() != R.id.divide;
                }
                catch (NumberFormatException ignored) {}
                resultButton.setEnabled(canCompute);
            }
            @Override
            public void afterTextChanged(Editable s) {
                // Si l'utilisateur commence par un point, on ajoute un 0 juste devant.
                if(s.toString().startsWith("."))
                    s.insert(0, "0");
            }
        };
        // Liaison entre les TextWatchers et les TextField correspondants.
        value1Field.addTextChangedListener(textWatcher1);
        value2Field.addTextChangedListener(textWatcher2);
        // Gestion du choix de calcul.
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            // On vérifie si les valeurs présentes sont correctes.
            if(checkFields()){
                // Vérification d'une potentielle division par 0.
                if(checkedId == R.id.divide)
                    canCompute=!((Double.parseDouble(value2Field.getText().toString())) == 0.0);
                else
                    canCompute = true;
            }
            resultButton.setEnabled(canCompute);
        });
        // Gestion du bouton de calcul.
        resultButton.setOnClickListener(v -> {
            // Vérification des valeurs.
            if(checkFields()){
                // Récupération du calcul souhaité par l'utilisateur.
                int checkedId = radioGroup.getCheckedRadioButtonId();
                if (checkedId == R.id.plus)
                    resultText.setText(String.valueOf(Double.parseDouble(value1Field.getText().toString()) + Double.parseDouble(value2Field.getText().toString())));
                else if (checkedId == R.id.minus)
                    resultText.setText(String.valueOf(Double.parseDouble(value1Field.getText().toString()) - Double.parseDouble(value2Field.getText().toString())));
                else if (checkedId == R.id.multiply)
                    resultText.setText(String.valueOf(Double.parseDouble(value1Field.getText().toString()) * Double.parseDouble(value2Field.getText().toString())));
                else if (checkedId == R.id.divide && canCompute)
                    resultText.setText(String.valueOf(Double.parseDouble(value1Field.getText().toString()) / Double.parseDouble(value2Field.getText().toString())));
                else
                    resultText.setText(R.string.noOperation);
            }
            // Si la valeur 1 est : manquante ou incorrecte...
            else if(!checkDoubleFromString(value1Field.getText().toString()))
                resultText.setText(R.string.val1Missing);
            // Sinon, il s'agit de la valeur 2 qui est : manquante ou incorrecte...
            else
                resultText.setText(R.string.val2Missing);
        });
        // Remise à Zero de la calculatrice.
        razButton.setOnClickListener(v -> {
            value1Field.setText("");
            value2Field.setText("");
            resultText.setText(R.string.defaultResultText);
            radioGroup.clearCheck();
        });
        // Sortie d'application.
        quitButton.setOnClickListener(v -> {
            finish();
            System.exit(0);
        });
    }

    /* Permet de vérifier les valeurs 1 et 2 de la calculatrice directement.
        True est retourné si les deux valeurs sont correctes.
        False sinon.
     */
    private boolean checkFields(){
        return (checkDoubleFromString(value1Field.getText().toString()) && checkDoubleFromString(value2Field.getText().toString()));
    }

    /* Permet de vérifier si une String contient bien un double.
    Pour ça, l'équivalent d'un try parse est réalisé.
    Si le try parse fonctionne, alors la string est correcte :
        -> True est retourné.
    Sinon, le try parse a échoué :
        -> False est retourné.
     */
    private boolean checkDoubleFromString(String value){
        try {
            tmpValue = Double.parseDouble(value);
            return true;
        }
        catch (NumberFormatException ignored) {
            return false;
        }
    }
}