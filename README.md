# CalcRingot

## Description

TP étudiant M2 : Calculatrice Android simple

## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus

### Mode portrait
<br>![Aperçu : Mode portrait](images/screenshot_portrait.png)

### Mode paysage
<br>![Aperçu : Mode paysage](images/screenshot_landscape.png)